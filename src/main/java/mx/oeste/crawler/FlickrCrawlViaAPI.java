package mx.oeste.crawler;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import mx.oeste.libs.FlickrPhoto;

import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.photos.GeoData;
import com.flickr4java.flickr.photos.Photo;
import com.flickr4java.flickr.photos.PhotoList;
import com.flickr4java.flickr.photos.SearchParameters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class FlickrCrawlViaAPI {

	private static Set<FlickrPhoto> fotosFlickr = new HashSet<>();
	private static HashSet hs = new HashSet();
	private static DateFormat dateFormat = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	private static String apiKey[] = { "37b07486d791f3d89b412dd2735eac8d",
			" db55ebd7dc8a05aba60a864ae557f732",
			"70e87c7e1e8ca865e9bd909eec1b9a5d" };
	private static String sharedSecret[] = { "c5c50cf25f6a297c",
			"1984074686800f00", "3b347d0ba525b5b2" };
	private static int apiPetition;
	private static int keyEnUso;

	List<String> locacionesHoteles;

	public FlickrCrawlViaAPI(List<String> locacionesHoteles) {
		apiPetition = 3600;
		keyEnUso = 0;
		this.locacionesHoteles = locacionesHoteles;
	}

	public String crawlFlickr() {

		// validar numero de peticiones restantes vs numeros de locaciones
		for (int index = 0; index < locacionesHoteles.size(); index++) {

			if (apiPetition < 1 && keyEnUso < apiKey.length
					&& index > ((keyEnUso + 1) * 36000)) {
				System.out
						.println("Limite de API Flickr, cambiando tokens de acceso...");
				apiPetition = 3600; // reinicia contador
				keyEnUso++;

				System.out.println("cambio de token flicker...");
			} else if (apiPetition < 1 && keyEnUso > apiKey.length
					&& index > ((keyEnUso + 1) * 36000)) {

				System.out.println("fin de claves...");
				return convertirJson(fotosFlickr);
			}

			getPhotos(locacionesHoteles.get(index));

		}

		return convertirJson(fotosFlickr);

	}

	private String convertirJson(Set<FlickrPhoto> fotosFlickr) {

		Gson gson = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting()
				.create();
		String json = gson.toJson(fotosFlickr);
		return json;

	}

	private static void getPhotos(String args) {
		String doc = "xx";
		// TODO Auto-generated method stub
		if (args == null)
			return;
		String tempCoordinates[] = args.split(",");

		if (tempCoordinates == null || tempCoordinates.length < 2)
			return;

		String lat = tempCoordinates[0];
		String lon = tempCoordinates[1];

		Date date = new Date();

		Calendar ca1endario = Calendar.getInstance();
		ca1endario.setTime(date);
		ca1endario.add(Calendar.DATE, -1);
		Date ayer = ca1endario.getTime();
		long unixTimeStamp = ayer.getTime() / 1000;

		String url = "https://api.flickr.com/services/rest/?&method=flickr.photos.search"
				+ "&api_key="
				+ apiKey[keyEnUso]
				+ "&min_upload_date="
				+ unixTimeStamp
				+ "&accuracy=16"
				+ "&safe_search=1"
				+ "&has_geo=1"
				+ "&lat="
				+ lat
				+ "&lon="
				+ lon
				+ "&radius=1&format=json&per_page=250&extras=geo,url_z,url_s";

		try {
			doc = Jsoup.connect(url).ignoreContentType(true).execute().body();
			apiPetition--;

			doc = doc.replace("/**/jsonFlickrApi(", "").replace(")", "");
			JSONObject obj;
			try {
				obj = new JSONObject(doc);
			} catch (JSONException o) {

				return;
			}

			//System.out.println(doc); //debug
			int totalFotos = obj.getJSONObject("photos").getInt("total");

			if (totalFotos == 0)
				return;

			JSONArray fotosLista = obj.getJSONObject("photos").getJSONArray(
					"photo");

			for (int i = 0; i < fotosLista.length(); i++) {

				JSONObject fotoObj = fotosLista.getJSONObject(i);

				try {
					fotoObj.getString("url_z");
				} catch (JSONException o) {

					i++;
					fotoObj = fotosLista.getJSONObject(i);
				}

				String id = fotoObj.getString("id");
				String title = fotoObj.getString("title");

				String fotoUrl = fotoObj.getString("url_z");
				String thumb = fotoObj.getString("url_s");
				double latd = fotoObj.getDouble("latitude");
				double lond = fotoObj.getDouble("longitude");

				FlickrPhoto foto = new FlickrPhoto();

				foto.setId(id);
				foto.setThumbnailUrl(thumb);
				foto.setTitulo(title);
				foto.setUrl(fotoUrl);
				foto.setLat(latd);
				foto.setLon(lond);

				if (!hs.contains(foto.getId())) {
						hs.add(foto.getId());
						fotosFlickr.add(foto);				
				}
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();

			return;
		}

	}

}
